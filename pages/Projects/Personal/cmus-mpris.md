cmus-mpris
==========
Python, D-Bus
-------------

A "plugin" for the C* Music Player to allow control over the D-Bus MPRIS specification. 

Source Code: https://gitlab.com/joonatoona/cmis-mpris