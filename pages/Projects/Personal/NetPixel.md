NetPixel
========
C++, Python, ESP8266
--------------------

Control an unlimited number of RGB LED strips wirelessly over the local network.

Source Code: https://gitlab.com/joonatoona/netpixel