ScreenTest
==========
C#, WinForms, C++, Arduino
--------------------------

Mirror the average colors of your monitor to an LED strip.

Source Code: https://github.com/joonatoona/ScreenTest