ArduController
==============
C++, Python
-----------

Program for the ArduBoy to emulate a keyboard and mouse.

Demo Video: https://peertube.fr/videos/watch/2a8414dd-52d6-4dd3-852f-e74317b9e70f  
Source Code: https://gitlab.com/joonatoona/ArduController