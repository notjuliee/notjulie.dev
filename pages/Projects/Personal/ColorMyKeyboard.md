ColorMyKeyboard
===============
Crystal, Javascript, Python, WebSockets
---------------------------------------

Control a Razer Chroma enabled keyboard over the internet in real time!

Demo Video: https://peertube.fr/videos/watch/4f7e6d94-d109-49bb-9d4a-66e096fd71cb  
Source Code: https://gitlab.com/joonatoona/ColorMyKeyboard