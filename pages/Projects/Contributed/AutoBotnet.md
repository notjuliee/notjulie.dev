AutoBotnet
==========
C#, C++
-------

AutoBotnet is an open source game centered on strategic programming, which
controls a more broad game based on resource managment and empire building.

One of it's primary goals is to be an educational but fun introduction to
programming through a massively multiplayer RTS game.

Website: https://autobotnet.pw  
Source Code: https://github.com/CookieEaters/AutoBotnet
