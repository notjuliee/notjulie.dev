Hearth Launcher
===============
C++, Qt5
--------

The Hearth Launcher is a Minecraft instance manager and mod pack installer.
Supports all major operating systems, and has an efficient and themeable UI.

Source Code: https://gitlab.com/HearthProject/HearthLauncher
