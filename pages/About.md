About Me
========
(Yes, I am taking some artistic liberty and rendering markdown in a terminal)

Hello, world! My name is Julia, and I'm a student studing computer science.
I like to make all kinds of cool things in my free time, some of which are listed
on this site in the Projects folder.

This site is a _single_ HTML file, that mirrors a directory with all these files in
it. Absolutely 0 JavaScript is used, thanks to some seriously disgusting CSS hacks
and lots of generated code.

(Ok well JS is used for small things like updating the window title and analytics, but
the core functionality of browsing a directory works even without JS enabled)

---

**Some links and stuff I guess:**

Email: julia@alt.icu  
GitLab: https://gitlab.com/joonatoona  
Gitea: https://git.alt.icu/julia  
Blog: https://blog.alt.icu/joonatoona
